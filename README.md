# AdviceSlips frontend

This is a frontend for the website [Advicebits](https://advicebits.com) which allows users to signup, authenticate their number, SMS messages, and send us contact messages.

It is built with React and Material UI.

  
## Getting Started

*Please make sure that you have `node` and `npm` installed.*

1. Clone this repository.
2. Navigate the frontend folder and install dependencies: `cd /frontend && npm install`
3. Start local dev server: `npm start`