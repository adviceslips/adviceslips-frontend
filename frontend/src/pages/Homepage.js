import React, { Fragment } from 'react';

// Components
import Quote from '../components/Quote';
import SignUp from '../components/SignUp';
import Contact from '../components/Contact';
import FAQ from '../components/FAQ';
import Unsubscribe from '../components/Unsubscribe';

// Material UI
import { Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

// Styles
import '../styles/homepage.css';

const useStyles = makeStyles({
    bodyContainer: { 
        maxWidth: '100%',
        marginTop: '15px',
        padding: '10px 15px 50px 15px',
        textAlign: 'center',
        fontSize: '17px',
        color: '#ffffff',
        backgroundColor: '#2e313e',
        boxSizing: 'border-box',
    }
});

function Homepage(props) {
    const classes = useStyles();
    const { currentView, updateCurrentView } = props;

    // Contact view
    const contactView = (
        <Fragment>
            <Paper
                id='viewContainer'
                elevation={5}
            >
                <Contact />
            </Paper>
        </Fragment>
    );

    // FAQ view
    const faqView = (
        <Fragment>
            <Paper
                id='viewContainer'
                elevation={5}
            >
                <FAQ updateCurrentView={updateCurrentView} />
            </Paper>
        </Fragment>
    )

    // Unsubscribe view
    const unsubscribeView = (
        <Fragment>
            <Paper
                id='viewContainer'
                elevation={5}
            >
                <Unsubscribe />
            </Paper>
        </Fragment>
    );

    // Default view
    const defaultView = (
        <Paper 
            className={classes.bodyContainer}
            elevation={1}
        >
            <div id='welcomeTitle'>Everybody needs some advice every now and again, let us text you it!</div>
            <div id='welcomeSubTitle'>Randomly generated advice sent Mon-Fri</div>

            <blockquote id='quoteContainer'><Quote /></blockquote>
            
            <div id='viewContainer' style={{ maxWidth: '430px' }}>
                <SignUp updateCurrentView={updateCurrentView} />
            </div>
        </Paper>
    );

    // Render what to display in Paper
    const renderView = () => {
        switch(currentView) {
            case 'Contact':
                return contactView;
            case 'FAQ':
                return faqView;
            case 'Unsubscribe':
                return unsubscribeView;
            default:
                return defaultView;
        }
    }

    return <Fragment>{renderView()}</Fragment>;
}

export default Homepage;
