import React, { useState } from 'react';

// Pages
import Homepage from '../pages/Homepage';

// Components
import Header from '../components/Header';
import Footer from '../components/Footer';

function App() {
    // Different view states
    const [currentView, updateCurrentView] = useState('Default');

    return (
        <div className="App">
            <Header updateCurrentView={updateCurrentView} />
            <Homepage currentView={currentView} updateCurrentView={updateCurrentView} />
            <Footer updateCurrentView={updateCurrentView} />
        </div>
    );
}

export default App;
