import React, { Fragment } from 'react';

// Styles
import '../styles/faq.css';

// Loads a random quote from advice slip api
function FAQ(props) {
    const { updateCurrentView } = props;

    return (
        <Fragment>
            <div id='faqTitle'>So you have some questions about us</div>
            <div id='faqSubTitle'>Let's see if we can answer some</div>

            <div id='questionsContainer'>
                <div className='questionText'>What do I need to join?</div>
                <div className='answerText'>Your name and a phone number! We also use 2FA to make sure no one tries to sign you up without your permission</div>
            </div>

            <div id='questionsContainer'>
                <div className='questionText'>What will you do with my number?</div>
                <div className='answerText'>Nothing! We will never give away your number to any 3rd parties</div>
            </div>

            <div id='questionsContainer'>
                <div className='questionText'>How many texts will I receive? Does it cost money?</div>
                <div className='answerText'>One per day, Monday - Friday, not too much, not too little. And we do not charge for this service</div>
            </div>

            <div id='questionsContainer'>
                <div className='questionText'>What type of advice is sent?</div>
                <div className='answerText'>Almost every type, really we have no direction. Sometimes we like to send a laugh, other times it can be that thing that reminds you about life</div>
            </div>

            <div id='questionsContainer'>
                <div className='questionText'>Want to donate to help spread the good vibes?</div>
                <div className='answerText'>
                    <div>For just 50 cents a day you can make sure a developer gets their redbull</div>
                    
                    <div id='donationContainer'>
                        Ways to help:
                        <div className='donationType'>Bitcoin - 34E74cTTNKBA1HLw1FrfqpRFifRL2m6d1W</div>
                        <div className='donationType'>Cashapp - $AndrewElick</div>
                        <div className='donationType'>Venmo - @AndrewElick</div>
                    </div>
                </div>
            </div>

            <div id='questionsContainer'>
                <div className='questionText'>How do I stop receiving texts?</div>
                <div className='answerText'>Click <span className='unsubscribeLink' onClick={() => updateCurrentView('Unsubscribe')}>here</span> and we'll help you go. Wander often, wonder always.</div>
            </div>
        </Fragment>
    );
}

export default FAQ;
