import React, { useState, useEffect } from 'react';

// Matieral UI
import { CircularProgress } from '@material-ui/core';

// Loads a random quote from advice slip api
function Quote() {
    const [loading, updateLoading] = useState(true);
    const [quote, updateQuote] = useState('');

    // Gets random quote from api
    const getRandomQuote = () => {
        fetch('https://api.adviceslip.com/advice')
            .then(response => response.json())
            .then(quote => {
                updateQuote(quote.slip.advice);
                updateLoading(false);
            })
    };

    useEffect(() => {
        getRandomQuote();
    }, [quote]);

    // Show loading icon
    if (loading) return <CircularProgress />

    return `"${quote}"`;
}

export default Quote;
