import React, { Fragment, useState } from 'react';

// Material UI
import { TextField, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PhoneNumber from 'awesome-phonenumber';

// Styles
import '../styles/unsubscribe.css';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            marginBottom: '10px',
            minWidth: '100%',
            maxWidth: '400px',
        },
        '& .MuiInputLabel-root': {
            color: '#ffffff',
            fontSize: '13px',
            fontFamily: 'inherit',
        },
        '& .MuiFilledInput-underline::before': {
            borderColor: '#2e313e'
        },
        '& MuiFilledInput-underline:hover': {
            borderColor: '#000000'
        },
        '& .MuiButton-root': {
            backgroundColor: '#1e1f28',
            justifyContent: 'center'
        },
        '& .MuiButton-root:hover': {
            backgroundColor: '#262833',
        },
        '& .MuiButton-root:disabled': {
            backgroundColor: '#50546b',
        },
        justifyContent: 'flex-end'
      
    },
    input: {
        color: '#ffffff',
        backgroundColor: '#616e7c',
        fontFamily: 'inherit',
    },
}));

function Unsubscribe() {
    const classes = useStyles();

    // User info states
    const [phoneNumber, updatePhoneNumber] = useState('');
    const [hasError, updateHasError] = useState(false);
    const [phoneError, updatePhoneError] = useState(false);
    const [submitError, updateSubmitError] = useState(false);
    const [submitSuccess, updateSubmitSuccess] = useState(false);

    const [loading, updateLoading] = useState(false);

    // Submit user info to api
    const submitRemoveUser = () => {
        const url = `https://advicebits.com/api/users/${phoneNumber}`;
        
        fetch(url, {
            method: 'DELETE',
        })
        .then(response => {
            if (![204].includes(response.status)) throw response;
            updateSubmitSuccess(true);
            updateLoading(false);
        })
        .catch(error => {
            console.log(error);
            updateSubmitError('There was an error while removing you from our list, please try again');
            updateLoading(false); 
        });
    }

    // Validate fields and send to api
    const handleSubmit = event => {
        // Prevent page refresh on enter key
        if (event) { 
            event.preventDefault();
        }

        // Clear old submit error
        updateSubmitError(false);

        if (phoneNumber === '') {
            updateHasError('Please enter phone number being used');
            updatePhoneError(true);
            return;
        } else if (!PhoneNumber(phoneNumber, 'US').isValid()) {
            updatePhoneError(true);
            return updateHasError('Not a valid phone number');
        }

        // Clear errors
        updatePhoneError(false);
        updateHasError(false);

        // Passed validation, diable button & send to api
        updateLoading(true);
        submitRemoveUser();
    }

    // Handle input update
    const handleInfoChange = event => {
        updatePhoneNumber(event.target.value);

        // Clear errors if good
        if (phoneNumber !== '' && PhoneNumber(phoneNumber, 'US').isValid()) {
            updatePhoneError(false)
            updateHasError(false)
        };
    }

    // User has successfully unsubscribed, hide form
    if (submitSuccess) {
        return (
            <Fragment>
                <div id='signUpTitle'>Tired of the advice?</div>
                <div id='successMessage'>
                    <img src='checkmark.png' alt='checkmark' id='successCheckmark' />
                    You have been unsubsribed. Were sad to see you go, 'But a wild creature will always go back to the wild, in the end'</div>
            </Fragment>
        );
    };

    return (
        <Fragment>
            <div id='unsubscribeTitle'>Tired of the advice?</div>
            <div id='unsubscribeSubTitle'>Enter your phone number and we'll remove you from the list</div>

            <form className={classes.root} name='unsubscribeForm' id='unsubscribeForm'>
                <TextField 
                    type='tel'
                    InputProps={{ className: classes.input }} 
                    name='phoneNumber' 
                    label='Phone Number' 
                    variant='filled'
                    onChange={handleInfoChange}
                    onKeyPress={(event) => event.key === 'Enter' ? handleSubmit(event) : null }
                    error= {phoneError}
                    autoComplete='off'
                    required
                />

                { hasError ? <div className='errorMessage' >{hasError}</div> : null }
                { submitError ? <div className='errorMessage' >{submitError}</div> : null }

                <Button 
                    type='button'
                    variant='contained'
                    color='primary'
                    onClick={handleSubmit}
                    classes={ classes.buttonProgress }
                    disabled={loading ? true : false}
                >
                    Unsubscribe
                </Button>
            </form>
        </Fragment>
    );
}

export default Unsubscribe;
