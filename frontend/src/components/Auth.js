import React, { Fragment, useState } from 'react';

// Material UI
import { TextField, Button, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

// Styles
import '../styles/auth.css';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            marginBottom: '10px',
            minWidth: '100%',
        },
        '& .MuiInputLabel-root': {
            color: '#ffffff',
            fontSize: '15px',
            fontFamily: 'inherit',
        },
        '& .MuiFilledInput-underline::before': {
            borderColor: '#2e313e'
        },
        '& MuiFilledInput-underline:hover': {
            borderColor: '#000000'
        },
        '& .MuiButton-root': {
            backgroundColor: '#1e1f28',
            justifyContent: 'center'
        },
        '& .MuiButton-root:hover': {
            backgroundColor: '#262833',
        },
        '& .MuiButton-root:disabled': {
            backgroundColor: '#50546b',
        },
        justifyContent: 'flex-end'
      
    },
    input: {
        color: '#ffffff',
        backgroundColor: '#616e7c',
        fontSize: '30px',
        fontFamily: 'inherit',
    },
}));


// View for users to send message back to us
function Auth(props) {
    const classes = useStyles();
    const { phoneNumber, updateAuthSuccess } = props;

    const tempPhoneNumer = phoneNumber ? phoneNumber : 9016667777;

    // Auth states
    const [authCode, updateAuthCode] = useState('');
    const [newAuthCodeMessage, updateNewAuthCodeMessage] = useState(false);
    
    // Validation states
    const [authCodeError, updateAuthCodeError] = useState(false);
    const [hasError, updateHasError] = useState(false);
    const [submitError, updateSubmitError] = useState(false);

    const [loading, updateLoading] = useState(false);
    const [sendingNewAuthCode, updateSendingNewAuthCode] = useState(false);

    // Submit auth info to api
    const submitUserInfo = () => {
        const url = `https://advicebits.com/api/users/auth/${tempPhoneNumer}/${authCode}`;
        
        fetch(url)
            .then(response => {
                // Catch errors
                if (![200,201].includes(response.status)) { 
                    throw response;
                } else {
                    // Tells parent to hide this and display success message
                    updateAuthSuccess(true);
                }
            })
            .catch(error => {
                // Bad auth code
                if (error.status === 401) {
                    // Get error message & convert to human friendly
                    error.json().then(error => {
                        const errorReason = error.reason;
                        let errorMessage;

                        // Choose human friendly error message
                        switch (errorReason) {
                            case 'provided authCode is invalid':
                                errorMessage = 'Code is invalid';
                                break;
                            case 'authCode has expired':
                                errorMessage = 'Code is expired';
                                break;
                            case 'User does not have any valid authCodes':
                                errorMessage = 'You have no valid codes, please request another';
                                break;
                            default:
                                console.log(errorReason);
                                errorMessage = 'Failed to validate code';
                                break;
                        }

                        // Show message and highlight red
                        updateSubmitError(errorMessage);
                        updateAuthCodeError(true);
                        updateLoading(false);
                    });
                } else {
                    // Unknown error happened
                    updateSubmitError('There was an error while authenticating phone, please try again');
                    updateLoading(false); 
                }
            });
    };

    // Send another auth code
    const requestNewAuthCode = () => {
        // Clear old message, show loading circle
        updateNewAuthCodeMessage(false);
        updateSendingNewAuthCode(true);

        const url = `https://advicebits.com/api/users/auth/${phoneNumber}/new`;

        fetch(url)
        .then(response => {
            if (![200,201].includes(response.status)) throw response;
            updateNewAuthCodeMessage('We sent a new code to your phone!');
            updateSendingNewAuthCode(false);
        })
        .catch(error => {
            updateNewAuthCodeMessage('Error while sending new code, please try again');
            updateSendingNewAuthCode(false); 
        });
    };

    // Validate fields and send to api
    const handleSubmit = event => {
        // Prevent page refresh on enter key
        if (event) { 
            event.preventDefault();
        }

        // Clear submit errors
        updateSubmitError(false);

        // Validate auth code
        if (authCode === '') {
            updateAuthCodeError(true);
            updateHasError('Please enter code');
            return;
        } else if (authCode.length !== 6) {
            updateAuthCodeError(true);
            updateHasError('Not a valid authentication code');
            return;
        }

        // Clear errors
        updateAuthCodeError(false);
        updateHasError(false);

        // Passed validation, diable button & send to api
        updateLoading(true);
        submitUserInfo();
    }

    // Handle input update
    const handleInfoChange = event => {
        updateAuthCode(event.target.value);

        // Clear error messages/highlighting if field good
        if (authCode !== '' && authCode.length === 6) {
            updateAuthCodeError(false);
            updateSubmitError(false);
        };
    }

    return (
        <Fragment>
            <div id='contactTitle'>Please confirm your number</div>
            <div id='contactSubTitle'>We sent an authentication code to you, please enter it here to start receiving your daily advice</div>

            <form className={classes.root} id='authForm'>
                <TextField 
                    type='tel'
                    InputProps={{ className: classes.input }} 
                    name='authCode' 
                    label='Code'
                    variant='filled'
                    onChange={handleInfoChange}
                    onKeyPress={(event) => event.key === 'Enter' ? handleSubmit(event) : null }
                    error= {authCodeError}
                    autoComplete='off'
                />

                { hasError ? <div className='errorMessage' >{hasError}</div> : null }
                { submitError ? <div className='errorMessage' >{submitError}</div> : null }

                <Button 
                    type='button'
                    variant='contained'
                    color='primary'
                    onClick={handleSubmit}
                    classes={ classes.buttonProgress }
                    disabled={loading ? true : false}
                >
                    Validate
                </Button>

                <div id='resendAuthContainer'>
                    Didn't get it? <span className='newAuthCodeLink' onClick={requestNewAuthCode}>Request a new code</span>
                    <div className='newAuthCodeMessage'>
                        { newAuthCodeMessage ? newAuthCodeMessage : null }
                        { sendingNewAuthCode ? <CircularProgress color='inherit' size={30} /> : null }
                    </div>
                </div>
            </form>
        </Fragment>
    );
}

export default Auth;
