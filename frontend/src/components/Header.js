import React from 'react';

// Styles
import '../styles/header.css';

function Header(props) {
    const { updateCurrentView } = props;

    return (
        <div id="headerContainer">
            <div id='headerLogo' onClick={() => updateCurrentView('Default')}>AdviceBits</div>
            
            <div id='headerLinksContainer'>
                <span name='FAQ' className='headerLink' onClick={() => updateCurrentView('FAQ')}>FAQ</span>
                <span name='Contact' className='headerLink' onClick={() => updateCurrentView('Contact')}>Contact</span>
            </div>
        </div>
    );
}

export default Header;
