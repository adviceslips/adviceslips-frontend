import React, { Fragment, useState } from 'react';

// Material UI
import { FormControl, FilledInput, InputLabel, TextField, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

// Email validation
import * as EmailValidator from 'email-validator';

// Styles
import '../styles/contact.css';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: '0 auto',
            marginBottom: '10px',
            minWidth: '100%',
            maxWidth: '350px',
        },
        '& .MuiFilledInput-root': {
            margin: '0 auto',
            marginBottom: '10px',
            minWidth: '100%',
            maxWidth: '350px',
        },
        '& .MuiInputLabel-root': {
            color: '#ffffff',
            fontSize: '13px',
            fontFamily: 'inherit',
        },
        '& .MuiFilledInput-underline::before': {
            borderColor: '#2e313e'
        },
        '& MuiFilledInput-underline:hover': {
            borderColor: '#000000'
        },
        '& .MuiButton-root': {
            backgroundColor: '#3e50b4',
            justifyContent: 'center'
        },
        '& .MuiButton-root:hover': {
            backgroundColor: '#5161bb',
        },
        '& .MuiButton-root:disabled': {
            backgroundColor: '#50546b',
        },
        justifyContent: 'flex-end'
      
    },
    input: {
        margin: '0 auto',
        marginBottom: '10px',
        minWidth: '100%',
        maxWidth: '350px',
        color: '#ffffff',
        backgroundColor: '#616e7c',
        fontFamily: 'inherit',
    },
}));


// View for users to send message back to us
function Contact(props) {
    const classes = useStyles();

    // User info states
    const [contactEmail, updateContactEmail] = useState('');
    const [contactMessage, updateContactMessage] = useState('');
    
    // Validation states
    const [contactEmailError, updateContactEmailError] = useState(false);
    const [contactMessageError, updateContactMessageError] = useState(false);
    const [hasError, updateHasError] = useState(false);
    const [submitError, updateSubmitError] = useState(false);
    const [submitSuccess, updateSubmitSuccess] = useState(false);

    const [loading, updateLoading] = useState(false);

    // Submit user info to api
    const submitUserInfo = () => {
        const url = `https://advicebits.com/api/messages/create`;
        
        fetch(url, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: contactEmail,
                message: contactMessage,
            }),
        })
        .then(response => {
            if (![200,201].includes(response.status)) throw response;
            updateSubmitSuccess(true);
            updateLoading(false);
        })
        .catch(error => {
            console.log(error);
            updateSubmitError('There was an error while sending message, please try again');
            updateLoading(false); 
        });
    }

    // Validate fields and send to api
    const handleSubmit = () => {
        // Clear submit errors
        updateSubmitError(false);

        if (contactEmail === '' || contactMessage === '') {
            updateHasError('Please fill out both fields');

            // Highlight field that is empty
            if (contactEmail === '') updateContactEmailError(true);
            if (contactMessage === '') updateContactMessageError(true);
            return;
        } else if (!EmailValidator.validate(contactEmail)) {
            updateContactEmailError(true);
            updateHasError('Not a valid email email address');

            // Clear message error if fine
            if (contactMessage !== '') updateContactMessageError(false);
            return;
        }

        // Clear errors
        updateContactEmailError(false);
        updateContactMessageError(false);
        updateHasError(false);

        // Passed validation, diable button & send to api
        updateLoading(true);
        submitUserInfo();
    }

    // Handle input update
    const handleInfoChange = event => {
        switch (event.target.name) {
            case 'contactEmail':
                updateContactEmail(event.target.value);
                break;
            case 'contactMessage':
                updateContactMessage(event.target.value);
                break;
            default:
                break;
        }

        // Clear error messages/highlighting if field(s) good
        if (contactMessage !== '') updateContactMessageError(false);
        if (contactEmail !== '' && EmailValidator.validate(contactEmail)) updateContactEmailError(false);
        if (contactEmail !== '' && contactMessage !== '' && EmailValidator.validate(contactEmail)) updateHasError(false);
    }

    // User has successfully signed up, hide form
    if (submitSuccess) {
        return (
            <Fragment>
                <div id='contactTitle'>Send us some advice, we need it</div>
                <div id='contactSubTitle'>We're always looking to improve</div>

                <div id='successMessage'>
                    <img src='checkmark.png' alt='checkmark' id='successCheckmark' />
                    Message has been sent, thanks for the feedback!
                </div>
            </Fragment>
        );
    };

    return (
        <Fragment>
            <div id='contactTitle'>Send us some advice, we need it</div>
            <div id='contactSubTitle'>We're always looking to improve</div>

            <form className={classes.root} name='contactForm' id='contactForm'>
                <FormControl variant='filled' fullWidth>
                    <InputLabel htmlFor="contactEmail">Email</InputLabel>
                    <FilledInput 
                        className={ classes.input }
                        type='email'
                        id='contactEmail'
                        name='contactEmail'
                        label='Email' 
                        variant='filled'
                        onChange={handleInfoChange}
                        error={contactEmailError}
                        required
                    />
                </FormControl>
                    
                <TextField 
                    type='text'
                    InputProps={{ className: classes.input }} 
                    name='contactMessage' 
                    label='Message' 
                    variant='filled'
                    onChange={handleInfoChange}
                    error= {contactMessageError}
                    rows={2}
                    rowsMax={20}
                    multiline
                    required
                />

                { hasError ? <div className='errorMessage' >{hasError}</div> : null }
                { submitError ? <div className='errorMessage' >{submitError}</div> : null }

                <Button 
                    variant='contained'
                    color='primary'
                    onClick={handleSubmit}
                    classes={ classes.buttonProgress }
                    disabled={loading ? true : false}
                >
                    Send
                </Button>
            </form>
        </Fragment>
    );
}

export default Contact;
