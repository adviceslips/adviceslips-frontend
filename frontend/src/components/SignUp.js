import React, { Fragment, useState } from 'react';

// Auth component, 2FA
import Auth from '../components/Auth';

// Material UI
import { TextField, Button, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PhoneNumber from 'awesome-phonenumber';

// Styles
import '../styles/signup.css';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: '0 auto',
            marginBottom: '10px',
            minWidth: '100%',
            maxWidth: '350px',
        },
        '& .MuiInputLabel-root': {
            color: '#ffffff',
            fontSize: '13px',
            fontFamily: 'inherit',
        },
        '& .MuiFilledInput-underline::before': {
            borderColor: '#2e313e'
        },
        '& MuiFilledInput-underline:hover': {
            borderColor: '#000000'
        },
        '& .MuiButton-root': {
            backgroundColor: '#3e50b4',
            justifyContent: 'center'
        },
        '& .MuiButton-root:hover': {
            backgroundColor: '#5161bb',
        },
        '& .MuiButton-root:disabled': {
            backgroundColor: '#50546b',
        },
        justifyContent: 'flex-end'
      
    },
    input: {
        color: '#ffffff',
        backgroundColor: '#616e7c',
        fontFamily: 'inherit',
    },
}));

function SignUp(props) {
    const classes = useStyles();
    const { updateCurrentView } = props;

    // User info states
    const [name, updateName] = useState('');
    const [phoneNumber, updatePhoneNumber] = useState('');
    const [hasError, updateHasError] = useState(false);
    const [nameError, updateNameError] = useState(false);
    const [phoneError, updatePhoneError] = useState(false);
    const [submitError, updateSubmitError] = useState(false);
    const [submitSuccess, updateSubmitSuccess] = useState(false);
    const [authSuccess, updateAuthSuccess] = useState(false);

    const [loading, updateLoading] = useState(false);

    // Submit user info to api
    const submitUserInfo = () => {
        const url = `https://advicebits.com/api/users/create`;
        
        fetch(url, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                name,
                phoneNumber
            }),
        })
        .then(response => {
            if (![200,201].includes(response.status)) throw response;
            updateSubmitSuccess(true);
            updateLoading(false);
        })
        .catch(error => {
            error.json().then(error => {
                const errorReason = error.reason;
                let errorMessage;

                // Choose human friendly error message
                switch (errorReason) {
                    case 'user already exists':
                        errorMessage = 'Phone number already in use';
                        break;
                    default:
                        console.log(errorReason);
                        errorMessage = 'Failed to add new user, please try again';
                        break;
                }

                updateSubmitError(errorMessage);
                updateLoading(false); 
            });
        });
    }

    // Validate fields and send to api
    const handleSubmit = () => {
        if (name === '' || phoneNumber === '') {
            updateHasError('Please fill out both fields');

            // Highlight field that is empty
            if (name === '') updateNameError(true);
            if (phoneNumber === '') updatePhoneError(true);
            return;
        } else if (!PhoneNumber(phoneNumber, 'US').isValid()) {
            updatePhoneError(true);
            return updateHasError('Not a valid phone number');
        }

        // Clear errors
        updateSubmitError(false);
        updateNameError(false);
        updatePhoneError(false);
        updateHasError(false);

        // Passed validation, diable button & send to api
        updateLoading(true);
        submitUserInfo();
    }

    // Handle input update
    const handleInfoChange = event => {
        switch (event.target.name) {
            case 'name':
                updateName(event.target.value);
                break;
            case 'phoneNumber':
                updatePhoneNumber(event.target.value);
                break;
            default:
                break;
        }

        // Clear error messages/highlighting if field(s) good
        if (name !== '') updateNameError(false);
        if (phoneNumber !== '' && PhoneNumber(phoneNumber, 'US').isValid()) updatePhoneError(false);
        if (name !== '' && phoneNumber !== '' && PhoneNumber(phoneNumber, 'US')) updateHasError(false);
    }

    // User has confirmed phone number, show success message
    if (submitSuccess && authSuccess) {
        return (
            <Fragment>
                <div id='signUpTitle'>
                    <img src='checkmark.png' alt='checkmark' id='successCheckmark' />
                    Phone number authenticated
                </div>
                <div id='successMessage'>
                    Congrats we've added you to the list, expect a text tomorrow!
                </div>
            </Fragment>
        );
    };

    // User has successfully signed up, show authentication form
    if (submitSuccess) {
        return <Auth phoneNumber={phoneNumber} updateAuthSuccess={updateAuthSuccess} />;
    };

    return (
        <Fragment>
            <div id='signUpTitle'>Want to join?</div>

            <form className={classes.root} name='signUpForm' id='signUpForm'>
                <TextField 
                    InputProps={{ className: classes.input }} 
                    name='name'
                    label='Name' 
                    variant='filled'
                    onChange={handleInfoChange}
                    onKeyPress={(event) => event.key === 'Enter' ? handleSubmit(event) : null }
                    error= {nameError}
                    autoComplete='off'
                    required
                />
                <TextField 
                    type='tel'
                    InputProps={{ className: classes.input }} 
                    name='phoneNumber' 
                    label='Phone Number' 
                    variant='filled'
                    onChange={handleInfoChange}
                    onKeyPress={(event) => event.key === 'Enter' ? handleSubmit(event) : null }
                    error= {phoneError}
                    autoComplete='off'
                    required 
                />

                { hasError ? <div className='errorMessage' >{hasError}</div> : null }
                { submitError ? <div className='errorMessage' >{submitError}</div> : null }
                { loading ? <CircularProgress color='inherit' size={30} /> : null }

                <Button 
                    variant='contained'
                    color='primary'
                    onClick={handleSubmit}
                    classes={ classes.buttonProgress }
                    disabled={loading ? true : false}
                >
                    Join
                </Button>
            </form>

            <div id='unsubsribeContainer'>
                Had your fill of advice? <span className='unsubscribeLink' onClick={() => updateCurrentView('Unsubscribe')}>Unsubscribe</span>
            </div>
        </Fragment>
    );
}

export default SignUp;
