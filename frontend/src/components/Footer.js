import React from 'react';

// Styles
import '../styles/footer.css';

function Footer(props) {
    const { updateCurrentView } = props;

    // Handle view update
    const handleUpdateView = view => updateCurrentView(view);

    return (
        <div id="footerContainer">
            <span name='Default' className='footerLink' onClick={() => handleUpdateView('Default')}>AdviceBits</span>
            <span name='FAQ' className='footerLink' onClick={() => handleUpdateView('FAQ')}>FAQ</span>
            <span name='Contact' className='footerLink' onClick={() => handleUpdateView('Contact')}>Contact</span>
            <a href='https://gitlab.com/adviceslips' target='_blank' rel='noopener noreferrer' name='Source' className='footerLink'>Code</a>
        </div>
    );
}

export default Footer;
